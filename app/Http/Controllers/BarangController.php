<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Barang;
use App\Models\Kategori;

class BarangController extends Controller
{
    public function index(): View
    {
        $barang = Barang::get();
        return view('barang.index', compact('barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('barang.tambah', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validasi data
        $request->validate([
            'nama_barang' => 'required'
        ]);
        //Memasukan data
        $barang = new Barang;

        $barang->nama_barang = $request['nama_barang'];

        $barang->save();

        // Pesan berhasil
        // Alert::success(' BERHASIL ', ' Berhasil Menambah barang! ');

        //kembali ke halaman index
        return redirect('/barang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id_barang)
    // {
    //     $barang = barang::find($id_barang);
    //     return view('barang.detail', compact('barang'));
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */

    public function edit(string $id_barang): View
    {
        $barang = Barang::findOrFail($id_barang);

        return view('barang.edit', compact('barang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_barang)
    {
        $validated = $request->validate([
            'nama_barang' => 'required'
        ]);

        $barang = Barang::find($id_barang);

        $barang->nama_barang = $request['nama_barang'];
        
        $barang->save();

        // Pesan berhasil
        // Alert::success(' BERHASIL ', ' Berhasil Mengubah barang! ');

        //kembali ke halaman index
        return redirect('/barang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_barang)
    {
        $barang = Barang::find($id_barang);
        $barang->delete();

        // Pesan berhasil
        // Alert::success(' BERHASIL ', ' Berhasil Menghapus barang! ');

        //kembali ke halaman index
        return redirect('/barang');
    }
}
