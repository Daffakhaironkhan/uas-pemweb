<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Kategori;
// use App\Http\Controllers\Alert;
// use App\Models\Topik;

class KategoriController extends Controller
{
    public function index(): View
    {
        $kategori = Kategori::get();
        return view('kategori.index', compact('kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategori.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validasi data
        $request->validate([
            'nama_kategori' => 'required'
        ]);
        //Memasukan data
        $kategori = new Kategori;

        $kategori->nama_kategori = $request['nama_kategori'];

        $kategori->save();

        // Pesan berhasil
        // Alert::success(' BERHASIL ', ' Berhasil Menambah Kategori! ');

        //kembali ke halaman index
        return redirect('/kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id_kategori)
    // {
    //     $kategori = Kategori::find($id_kategori);
    //     return view('kategori.detail', compact('kategori'));
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */

    public function edit(string $id_kategori): View
    {
        $kategori = Kategori::findOrFail($id_kategori);

        return view('kategori.edit', compact('kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_kategori)
    {
        $validated = $request->validate([
            'nama_kategori' => 'required'
        ]);

        $kategori = Kategori::find($id_kategori);

        $kategori->nama_kategori = $request['nama_kategori'];
        
        $kategori->save();

        // Pesan berhasil
        // Alert::success(' BERHASIL ', ' Berhasil Mengubah Kategori! ');

        //kembali ke halaman index
        return redirect('/kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_kategori)
    {
        $kategori = Kategori::find($id_kategori);
        $kategori->delete();

        // Pesan berhasil
        // Alert::success(' BERHASIL ', ' Berhasil Menghapus Kategori! ');

        //kembali ke halaman index
        return redirect('/kategori');
    }
}
