<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Supplier;

class SupplierController extends Controller
{
    public function index(): View
    {
        $supplier = Supplier::get();
        return view('supplier.index', compact('supplier'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('supplier.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validasi data
        $request->validate([
            'nama_supplier' => 'required'
        ]);
        //Memasukan data
        $supplier = new Supplier;

        $supplier->nama_supplier = $request['nama_supplier'];

        $supplier->save();

        // Pesan berhasil
        // Alert::success(' BERHASIL ', ' Berhasil Menambah Supplier! ');

        //kembali ke halaman index
        return redirect('/supplier');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id_supplier)
    // {
    //     $supplier = Supplier::find($id_supplier);
    //     return view('supplier.detail', compact('supplier'));
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */

    public function edit(string $id_supplier): View
    {
        $supplier = Supplier::findOrFail($id_supplier);

        return view('supplier.edit', compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_supplier)
    {
        $validated = $request->validate([
            'nama_supplier' => 'required'
        ]);

        $supplier = Supplier::find($id_supplier);

        $supplier->nama_supplier = $request['nama_supplier'];
        
        $supplier->save();

        // Pesan berhasil
        // Alert::success(' BERHASIL ', ' Berhasil Mengubah Supplier! ');

        //kembali ke halaman index
        return redirect('/supplier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_supplier)
    {
        $supplier = Supplier::find($id_supplier);
        $supplier->delete();

        // Pesan berhasil
        // Alert::success(' BERHASIL ', ' Berhasil Menghapus Supplier! ');

        //kembali ke halaman index
        return redirect('/supplier');
    }
}
