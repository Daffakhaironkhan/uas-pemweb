@extends('layout.master')

@section('title')
    Halaman Edit Data Barang
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="card">
                <div class="card-body">
                    <h2>Formulir Edit Barang</h2>
                    <form action="/barang/{{$barang->id_barang}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="nama_barang">Nama Barang</label>
                            <input type="text" class="form-control" id="nama_barang" name='nama_barang' value="{{$barang->nama_barang}}">
                        </div>
                        @error('nama_barang')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group">
                            <label for="id_kategori">Kategori</label>
                            <select class="form-control" name="id_kategori" id="id_kategori">
                                <option value="">--Pilih Kategori--</option>
                                @forelse ($kategori as $item)
                                      @if ($item->id_kategori === $barang->id_kategori)
                                          <option value="{{$item->id_kategori}}" selected>{{$item->nama_kategori}}</option>    
                                      @else
                                          <option value="{{$item->id_kategori}}">{{$item->nama_kategori}}</option>                     
                                      @endif
                                @empty
                                    <option value="">Belum ada data kategori</option>
                                @endforelse
                            </select>
                        </div>
                        @error('id_kategori')
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection