@extends('layout.master')

@section('title')
    Halaman Edit Data Kategori
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="card">
                <div class="card-body">
                    <h2>Formulir Edit Kategori</h2>
                    <form action="/kategori/{{$kategori->id_kategori}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="nama_kategori">Nama kategori</label>
                            <input type="text" class="form-control" id="nama_kategori" name='nama_kategori' value="{{$kategori->nama_kategori}}">
                        </div>
                        @error('nama_kategori')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection