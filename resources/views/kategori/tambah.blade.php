@extends('layout.master')

@section('title')
    Halaman Tambah Data Kategori
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="card">
                <div class="card-body">
                    <h2>Formulir Tambah Kategori</h2>
                    <form action="/kategori" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="nama_kategori">Nama Kategori</label>
                            <input type="text" class="form-control" id="nama_kategori" name='nama_kategori'>
                        </div>
                        @error('nama_kategori')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection