<div class="nk-sidebar">           
    <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">
            <li class="nav-label">Table</li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-grid menu-icon"></i><span class="nav-text">Table</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="/kategori">Kategori</a></li>
                    <li><a href="/barang">Barang</a></li>
                    <li><a href="/supplier">Supplier</a></li>
                    <li><a href="/penerimaan">Penerimaan</a></li>
                </ul>
            </li>
            <li class="nav-label">Pages</li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-notebook menu-icon"></i><span class="nav-text">Pages</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="{{route('profile.edit')}}">Profile</a></li>
                    <li><a href="{{route('logout')}}">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>