@extends('layout.master')

@section('title')
    Halaman Data Supplier
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.13.1/datatables.min.css" />
@endpush
@push('scripts')
    <script src="{{ asset('/dashboardtemplate/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('/dashboardtemplate/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#supplierTables").DataTable();
        });
    </script>
@endpush

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg">
                <div class="card">
                    <div class="card-body">
                        <a href="/supplier/create" type="button" class="btn btn-success mb-3">Tambah</a>
                        <table id="supplierTables" class="table table-bordered table-striped data">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Nama Supplier</th>
                                    <th scope="col">
                                        <center>Opsi</center>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($supplier as $key => $item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->nama_supplier}}</td>
                                        <td>
                                            <center>
                                                <form action="/supplier/{{ $item->id_supplier}}" method='post'>
                                                    @csrf
                                                    @method('delete')
                                                    <a href="/supplier/{{ $item->id_supplier}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                                    <button type="submit" class="btn btn-danger btn-sm show_confirm">Hapus</button>
                                                </form>
                                            </center>
                                        </td>
                                    </tr>
                                @empty
                                    <div class="alert alert-danger">
                                        Data Supplier belum Tersedia.
                                    </div>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript">
            $('.show_confirm').click(function(event) {
                var form = $(this).closest("form");
                var name = $(this).data("name");
                event.preventDefault();
                swal({
                        title: `Anda yakin menghapus data ini?`,
                        text: "Klik OK untuk menghapus",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            form.submit();
                        }
                    });
            });
        </script>
    @endpush
@endsection