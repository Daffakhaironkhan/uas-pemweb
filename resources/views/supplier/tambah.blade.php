@extends('layout.master')

@section('title')
    Halaman Tambah Data Supplier
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="card">
                <div class="card-body">
                    <h2>Formulir Tambah Supplier</h2>
                    <form action="/supplier" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="nama_supplier">Nama Supplier</label>
                            <input type="text" class="form-control" id="nama_supplier" name='nama_supplier'>
                        </div>
                        @error('nama_supplier')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection